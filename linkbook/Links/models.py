from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Link(models.Model):
    title = models.CharField(null=False, blank=False, max_length=800)
    url = models.URLField(null=False, blank=False)
    creator = models.ForeignKey(User, null=True, blank=False, default=None, on_delete=models.CASCADE)
    is_deactivated = models.BooleanField(default=False)
    created_on = models.DateTimeField(auto_now_add=True)

