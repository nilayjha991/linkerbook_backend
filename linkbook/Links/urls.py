from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import *

urlpatterns = [
    url(r'signup/$', RegisterUser.as_view()),
    url(r'signin/$', Login.as_view()),
    url(r'link/$', LinkPostAndGetAll.as_view()),
    url(r'link/(?P<link_id>[0-9]+)/$', LinkPutDelGet.as_view()),
    ]

urlpatterns = format_suffix_patterns(urlpatterns)