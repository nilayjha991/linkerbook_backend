import requests

# this user has no email
# {
#         "id": 199,
#         "user_username": "delete"
#     },

if __name__ == "__main__":
    # feed boss details in the list below
    boss_details = [
        {
            "id": 184,
            "user_username": "trevor@courtland.biz"
        },
        {
            "id": 185,
            "user_username": "pppfarms@ciaccess.com"
        },
        {
            "id": 186,
            "user_username": "er-liacres@outlook.com"
        },
        {
            "id": 187,
            "user_username": "ryan@kingsenergygroup.com"
        },
        {
            "id": 188,
            "user_username": "bpy@townshipofbrock.ca"
        },
        {
            "id": 189,
            "user_username": "jfl@sinistre.com"
        },
        {
            "id": 190,
            "user_username": "randy@greenleaffuel.com"
        },
        {
            "id": 191,
            "user_username": "denise.stasiuk@conaresources.com"
        },
        {
            "id": 192,
            "user_username": "ironinventory@hotmail.com"
        },
        {
            "id": 193,
            "user_username": "bill.dodd.wd@gmail.com"
        },
        {
            "id": 194,
            "user_username": "sarahyeo@intragrain.com"
        },
        {
            "id": 195,
            "user_username": "info@serkka.com"
        },
        {
            "id": 196,
            "user_username": "joshuayoung@swconcreteconst.com"
        },
        {
            "id": 197,
            "user_username": "mmt@quadro.net"
        },
        {
            "id": 198,
            "user_username": "jerrydrudge@gmail.com"
        },
        {
            "id": 200,
            "user_username": "info@ocpltd.ca"
        },
        {
            "id": 201,
            "user_username": "skylerduncan@sundrecontracting.com"
        },
        {
            "id": 202,
            "user_username": "martin.walder@hbcturin.com"
        },
        {
            "id": 203,
            "user_username": "martin.waldner@hbcturin.com"
        },
        {
            "id": 204,
            "user_username": "pwadmin@northernbruce.ca"
        },
        {
            "id": 205,
            "user_username": "dave@drfreightservice.com"
        },
        {
            "id": 206,
            "user_username": "gkerr@intragrain.com"
        },
        {
            "id": 207,
            "user_username": "grantkerr@intragrain.com"
        },
        {
            "id": 208,
            "user_username": "mjaj99@hotmail.com"
        },
        {
            "id": 209,
            "user_username": "info@sewertechnologies.com"
        },
        {
            "id": 210,
            "user_username": "danimal.thorson+2.12tes@gmail.com"
        },
        {
            "id": 211,
            "user_username": "danimal.thorson+2.12ca@gmail.com"
        },
        {
            "id": 212,
            "user_username": "carl@veikleagro.com"
        },
        {
            "id": 213,
            "user_username": "jeff.bredy@parkland.ca"
        },
        {
            "id": 214,
            "user_username": "burmfarms@hotmail.com"
        },
        {
            "id": 215,
            "user_username": "Laramiedoud@yahoo.ca"
        },
        {
            "id": 228,
            "user_username": "john@borthag.com"
        },
        {
            "id": 229,
            "user_username": "jbrunet@ucrs.ca"
        },
        {
            "id": 230,
            "user_username": "jaymeoliver@storm.ca"
        },
        {
            "id": 231,
            "user_username": "calsay123@gmail.com"
        },
        {
            "id": 232,
            "user_username": "rklingenberg@swox.org"
        },
        {
            "id": 233,
            "user_username": "williamdd@naturefeedcentre.ca"
        },
        {
            "id": 234,
            "user_username": "neilrats@yahoo.ca"
        },
        {
            "id": 235,
            "user_username": "darrenneilandsonsexcavating@gmail.com"
        },
        {
            "id": 236,
            "user_username": "brivay@yahoo.com"
        },
        {
            "id": 237,
            "user_username": "scott@roslin.ca"
        },
        {
            "id": 238,
            "user_username": "saiah.peters@gmail.com"
        },
        {
            "id": 239,
            "user_username": "mfc@moran.ca"
        },
        {
            "id": 240,
            "user_username": "walker@southernspur.com"
        },
        {
            "id": 241,
            "user_username": "mikecarefoot@gmail.com"
        },
        {
            "id": 242,
            "user_username": "noal.swanson@cnrl.com"
        },
        {
            "id": 243,
            "user_username": "guy.schappert@CNRL.com"
        },
        {
            "id": 244,
            "user_username": "tbrown@energysolutions.com"
        },
        {
            "id": 245,
            "user_username": "ldnelson@platinum.ca"
        },
        {
            "id": 246,
            "user_username": "db27@bar2trucking.com"
        },
        {
            "id": 247,
            "user_username": "patti@bre-ex.com"
        },
        {
            "id": 248,
            "user_username": "randy.guimond@scotts.com"
        },
        {
            "id": 249,
            "user_username": "tim@gasserag.com"
        },
        {
            "id": 250,
            "user_username": "dylon@trpetro.com"
        },
        {
            "id": 251,
            "user_username": "lisa@devolderfarms.com"
        },
        {
            "id": 252,
            "user_username": "davidmccallum@start.ca"
        },
        {
            "id": 253,
            "user_username": "accounting@whiteoutgroup.ca"
        },
        {
            "id": 254,
            "user_username": "sam@groundtech.ca"
        },
        {
            "id": 255,
            "user_username": "donald.geddes@fourrivers.crs"
        },
        {
            "id": 256,
            "user_username": "dcharbonneau@millsap.ca"
        }
    ]
    # the period in milliseconds for which
    # reports have to be generated
    start_date = 1612159200000
    end_date = 1614578400000
    # API endpoint to generate and email usage reports
    target_url = "https://ws.fuellock.ca:7777/EmailService/webresources/EmailService/sendEmailAndGenerateReport/{0}/{1}/{2}"
    for boss_detail in boss_details:
        data = {
            "subjectLine": "Your Fuel Lock Report",
            "emailListTo": [boss_detail['user_username']],
            "emailParagraphList": ["Usage Report for the month of February 2021"],
            "emailParagraphHtmlList": ["<b>Usage Report for the month of February 2021</b>"]
        }
        boss_report_url = target_url.format(boss_detail['id'], start_date, end_date)
        print("########################################################################################################")
        print("generating report for username: {0}, user_id: {1}".format(boss_detail['user_username'], boss_detail['id']))
        print("--------------------------------------------------------------------------------------------------------")
        try:
            res = requests.post(boss_report_url, data)
            print(res.text)
        except Exception as ex:
            print(ex.message)
            print("--------------------------------------------------------------------------------------------------------")