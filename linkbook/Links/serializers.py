from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
from .models import *


class LinksSerializer(ModelSerializer):
    class Meta:
        model = Link
        fields = '__all__'
        read_only_fields = ('id', 'created_on')


class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        read_only_fields = ('id',)


class RegisterRequestSerializer(serializers.Serializer):
    email = serializers.EmailField(allow_blank=False, allow_null=False)
    password = serializers.CharField(allow_null=False, allow_blank=False)
    first_name = serializers.CharField(allow_null=False, allow_blank=False)
    last_name = serializers.CharField(allow_null=False, allow_blank=False)