from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework import status
from .serializers import *
from rest_framework import permissions


# Create your views here.


class RegisterUser(APIView):

    permission_classes = ()

    def post(self, request):
        print(request.data)
        register_ser = RegisterRequestSerializer(data=request.data)
        if register_ser.is_valid():
            request.data['username'] = '{0} {1}'.format(request.data['first_name'], request.data['last_name'])
            password = request.data['password']
            del request.data['password']
            user = User(**request.data)
            user.set_password(password)
            user.save()
            token = Token.objects.create(user=user)

            return Response(data={
                "user": {
                        "id": user.id,
                        "first_name": user.first_name,
                        "last_name": user.last_name,
                        "username": user.username
                        },
                "token": token.key
            }, status=status.HTTP_201_CREATED)
        else:
            return Response(data={'error': register_ser.errors.__dict__}, status=status.HTTP_400_BAD_REQUEST)


class Login(APIView):

    permission_classes = ()

    def post(self, request):
        user = User.objects.filter(email=request.data["email"])
        if not user.exists():
            return Response(data={"error": "invalid credentials"}, status=status.HTTP_401_UNAUTHORIZED)
        else:
            user = user.first()
            if not user.check_password(request.data["password"]):
                return Response(data={"error": "invalid credentials"}, status=status.HTTP_401_UNAUTHORIZED)
            else:
                token = Token.objects.get(user=user)
                return Response(data={
                    "user": {
                        "id": user.id,
                        "first_name": user.first_name,
                        "last_name": user.last_name,
                        "username": user.username
                        },
                    "token": token.key
                }, status=status.HTTP_200_OK)


class LinkPostAndGetAll(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request):
        request.data['creator'] = request.user.id
        link_ser = LinksSerializer(data=request.data)
        if link_ser.is_valid():
            link_ser.save()
            return Response(data=LinksSerializer(link_ser.instance).data, status=status.HTTP_201_CREATED)
        else:
            return Response(data=link_ser.errors.__dict__, status=status.HTTP_400_BAD_REQUEST)

    def get(self, request):
        link_ser = LinksSerializer(instance=Link.objects.filter(creator=request.user), many=True)
        return Response(data=link_ser.data, status=status.HTTP_200_OK)


class LinkPutDelGet(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def get(self, request, link_id):
        link = Link.objects.filter(id=link_id, creator=request.user).first()
        if not link:
            return Response(data={"error": "link not found"}, status=status.HTTP_404_NOT_FOUND)
        return Response(data=LinksSerializer(instance=link).data, status=status.HTTP_200_OK)

    def put(self, request, link_id):
        link = Link.objects.filter(id=link_id, creator=request.user).first()
        if not link:
            return Response(data={"error": "link not found"}, status=status.HTTP_404_NOT_FOUND)

        link_ser = LinksSerializer(data=request.data, partial=True)

        if not link_ser.is_valid():
            return Response(data={"errors": link_ser.errors.__dict__}, status=status.HTTP_200_OK)

        link_obj = link_ser.update(instance=link, validated_data=link_ser.validated_data)
        return Response(data=link_ser.to_representation(link_obj), status=status.HTTP_200_OK)

    def delete(self, request, link_id):
        link = Link.objects.filter(id=link_id, creator=request.user).first()
        if not link:
            return Response(data={"error": "link not found"}, status=status.HTTP_404_NOT_FOUND)

        link.objects.delete(id=link_id)
        return Response(data={"message": "link deleted successfully"}, status=status.HTTP_200_OK)
